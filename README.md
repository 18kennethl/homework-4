# CSE 109 - Homework 4

You will implement a hash set data structure in this assignment. The core of the hashset is an array of linked list pointers. The type of the table is a List**. The first star indicates it's a pointer to an array, the second star indicates each array element holds a List pointer. The Hashset also holds its size, and the current load factor. The load factor will be recalculated on each insert, and if the value exceeds a threshold (70% filled buckets) then the underlying table array needs to be resized. This will involve allocating a new range of memory and rehashing all of the elements into this new array, then freeing the old array and its constituent nodes and linked lists.

The Hashset struct is declared in `hashset.h`, along with a number of functions you will need to implement.

```c
// Generate a prehash for an item with a given size
unsigned long prehash(void* item, unsigned int item_size);

// Hash an unsigned long into an index that fits into a hash set
unsigned long hash(unsigned long prehash, unsigned long buckets);

// Initialize an empty hash set with a given size
void initHashSet(HashSet* hashset_pointer, unsigned int size);

// Insert item in the set. Return true if the item was inserted, false if it wasn't (i.e. it was already in the set)
// Recalculate the load factor after each successful insert (round to nearest whole number).
// If the load factor exceeds 70 after insert, resize the table to hold twice the number of buckets.
bool insertItem(HashSet* hashset_pointer, void* item, unsigned int item_size);

// Remove an item from the set. Return true if it was removed, false if it wasn't (i.e. it wasn't in the set to begin with)
bool removeItem(HashSet* hashset_pointer, void* item, unsigned int item_size);

// Return true if the item exists in the set, false otherwise
bool findItem(HashSet* hashset_pointer, void* item, unsigned int item_size);

// Resize the underlying table to the given size. Recalculate the load factor after resize
void resizeTable(HashSet* hashset_pointer, unsigned int new_size);

// Print Table
void printHashSet(HashSet* hashset_pointer);
```

## Build Instructions

Write build instructions here.

## Usage

Write usage instructions here. Be sure how to tell the user how to free the memory allocated for the hash set so that no memory leaks will occur.

## Examples

Write two examples demonstrating how to use the hash set struct.

## Assignment Instructions

1. Fork the relevant repository into your CSE 109 group namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)

2. Clone your newly forked repository to your computer. Your repository should be hosted at 
```
https://gitlab.com/<your user name>-cse109/<assignment name>
```
You can use the following git command with the appropriate values substituted to customize it for you:
```
git clone https://gitlab.com/<your user name>-cse109/<assignment name>
```
[Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 

3. Write the necessary code to get your project to pass the tests in `src/bin/tests.c`. Do not modify this file. As you work on your project, you can use `src/bin/main.c` to write any scratch code you need to test your assignment as you're working on it.

4. Commit the changes you made locally to your gitlab repository. Follow the instructions [here](https://githowto.com/staging_and_committing) (read sections 6, 7 and 8) about staging and committing changes.

5. Check the status of your commit with the appropriate values substituted to customize it for you at: 
```
https://gitlab.com/<your user name>-cse109/<assignment name>/pipelines
```